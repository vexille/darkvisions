﻿using UnityEngine;
using System.Collections;

public class HangingInputHandler : InputHandler {

    private Player player;

    public HangingInputHandler(Player player) {
        this.player = player;
    }

    public override void HandleInput() {
        float v = Input.GetAxis("Vertical");
        if (v < 0f) {
            player.SetState(new PlayerFallingState(player));

            Animator anim = this.player.gameObject.transform.Find("AFuckupOffset").GetComponentInChildren<Animator>();
            if (anim != null) {
                anim.SetTrigger("Release");
            }
        }

        if (v > 0f) {
            player.SetState(new PlayerClimbingState(player, true));
        }
    }

    public override void HandleInputFixed() {
        
    }
	
}
