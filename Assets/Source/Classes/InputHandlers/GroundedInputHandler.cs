﻿using UnityEngine;
using System.Collections;

public class GroundedInputHandler : InputHandler {

    private Player player;
    private MoveCommand moveCommand;
    private JumpCommand jumpCommand;
    private bool playerJumped;

    public GroundedInputHandler(Player player) {
        this.player = player;
        this.moveCommand = new MoveCommand();
        this.jumpCommand = new JumpCommand();
        this.playerJumped = false;

        this.moveCommand.MoveForce = player.moveForce;
        this.moveCommand.MaxSpeed = player.maxSpeed;
        this.moveCommand.MaxRunSpeed = player.maxRunSpeed;
    }

    public override void HandleInput() {
        GroundCheck groundCheck = player.GetComponentInChildren<GroundCheck>();

        if (groundCheck != null && groundCheck.Grounded && Input.GetButtonDown("Jump")) {
            this.playerJumped = true;
            this.jumpCommand.JumpForce = player.jumpForce;
            this.player.CurrentState.AddCommand(jumpCommand, true);
        }

        TriggerChecker triggerChecker = this.player.GetComponent<TriggerChecker>();
        if (triggerChecker != null) {

            if (triggerChecker.TouchingDoor && Input.GetButtonDown("EnterDoor"))
                Application.LoadLevel(SceneControl.Instance.nextLevel);

            if (triggerChecker.TouchingAltar && Input.GetButtonDown("Chant")) {
                this.player.SetState(new PlayerChantingState(this.player));
            }
        }


    }

    public override void HandleInputFixed() {
        if (!playerJumped) {
            GroundCheck groundCheck = player.GetComponentInChildren<GroundCheck>();
            if (groundCheck != null && groundCheck.Grounded) {
                this.moveCommand.Horizontal = Input.GetAxis("Horizontal");
                this.moveCommand.Running = Input.GetButton("Run");

                player.CurrentState.AddCommand(this.moveCommand, true);

                this.jumpCommand.Running = this.moveCommand.Running;
            }
        } else {
            playerJumped = false;
        }
    }
	
}
