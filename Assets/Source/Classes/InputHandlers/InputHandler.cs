﻿using UnityEngine;
using System.Collections;

abstract public class InputHandler {

    abstract public void HandleInput();
    abstract public void HandleInputFixed();
	
}
