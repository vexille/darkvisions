﻿using UnityEngine;
using System.Collections;

public class ChantingInputHandler : InputHandler {

    private Player player;
    private ChantCommand chantCommand;

    public ChantingInputHandler(Player player) {
        this.player = player;
        this.chantCommand = new ChantCommand();
    }

    public override void HandleInput() {
        if (Input.GetButton("Chant")) {
            this.player.CurrentState.AddCommand(chantCommand);
        } else {
            this.player.SetState(new PlayerGroundedState(player));
        }
    }

    public override void HandleInputFixed() {

    }

}
