﻿using UnityEngine;
using System.Collections;

public class State {

    protected Player player;
    protected InputHandler inputHandler;
    protected CommandQueue commandQueue;
    
    public State(Player player, InputHandler inputHandler, CommandQueue commandQueue) {
        this.player = player;
        this.inputHandler = inputHandler;
        this.commandQueue = commandQueue;
    }

    public virtual void Update() {
        if (this.inputHandler != null)
            this.inputHandler.HandleInput();
        else
            Debug.LogError("Input handler NULL");

        if (this.commandQueue != null)
            this.commandQueue.Update();
    }

    public virtual void FixedUpdate() {
        if (this.inputHandler != null)
            this.inputHandler.HandleInputFixed();

        if (this.commandQueue != null)
            this.commandQueue.FixedUpdate();
    }

    public virtual void OnEnter() {

    }

    public virtual void OnExit() {

    }

    public void AddCommand(Command command, bool fixedUpdate = false) {
        CommandQueue.AddCommand(command, fixedUpdate);
    }

    public CommandQueue CommandQueue {
        get { return this.commandQueue; }
    }

}
