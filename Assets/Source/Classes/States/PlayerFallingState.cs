﻿using UnityEngine;
using System.Collections;

public class PlayerFallingState : State {

    private GroundCheck groundChecker;

    public PlayerFallingState(Player player)
        : base(player, null, null) {
            groundChecker = player.GetComponentInChildren<GroundCheck>();
    }

    public override void Update() {
        if (this.groundChecker != null && this.groundChecker.Grounded) {
            player.SetState(new PlayerGroundedState(base.player));
        }
    }

    public override void FixedUpdate() {
        base.FixedUpdate();
    }

    public override void OnEnter() {
    }

    public override void OnExit() {
    }
	
}
