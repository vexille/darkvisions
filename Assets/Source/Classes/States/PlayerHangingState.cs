﻿using UnityEngine;
using System.Collections;

public class PlayerHangingState : State {

    private float startingGravityScale;

    public PlayerHangingState(Player player)
        : base(player, new HangingInputHandler(player), new CommandQueue(player.gameObject)) {
        
    }

    public override void Update() {
        base.Update();
    }

    public override void FixedUpdate() {
        base.FixedUpdate();
    }

    public override void OnEnter() {
        Animator anim = base.player.gameObject.transform.Find("AFuckupOffset").GetComponentInChildren<Animator>();
        if (anim != null) {
            anim.SetTrigger("Hanging");
        }

        Rigidbody2D rigidbody = base.player.GetComponent<Rigidbody2D>();
        if (rigidbody != null) {
            this.startingGravityScale = rigidbody.gravityScale;
            rigidbody.gravityScale = 0f;
            rigidbody.velocity = new Vector2(0f, 0f);
        }
    }

    public override void OnExit() {
        Rigidbody2D rigidbody = base.player.GetComponent<Rigidbody2D>();
        if (rigidbody != null) {
            rigidbody.gravityScale = this.startingGravityScale;
        }
    }
	
}
