﻿using UnityEngine;
using System.Collections;

public class PlayerGroundedState : State {

    public PlayerGroundedState(Player player)
        : base(player, new GroundedInputHandler(player), new CommandQueue(player.gameObject)) {
        
    }

    public override void Update() {
        base.Update();
    }

    public override void FixedUpdate() {
        LedgeCheck ledgeChecker = base.player.GetComponentInChildren<LedgeCheck>();
        if (ledgeChecker != null && ledgeChecker.TouchingLedge) {
            //base.player.SetState(new PlayerHangingState(base.player));
            ChangeToHangingState(ledgeChecker.LedgeCollider);
            return;
        }
        base.FixedUpdate();
    }

    public override void OnEnter() {

    }

    private void ChangeToHangingState(Collider2D ledgeCollider) {
        Vector3 playerPos = base.player.transform.position;
        Vector3 ledgePos = ledgeCollider.transform.position;
        Bounds ledgeFloorBounds = ledgeCollider.renderer.bounds;

        float xDisplacement = (ledgeFloorBounds.size.x / 2f) * Mathf.Sign(ledgeCollider.transform.localPosition.x);
        //Debug.Log("Player pos pre: " + playerPos.x + ", " + playerPos.y);

        playerPos.y = ledgePos.y - 1.803438f;
        playerPos.x = ledgePos.x + xDisplacement;
        base.player.transform.position = playerPos;

        //Debug.Log("Player pos pos: " + playerPos.x + ", " + playerPos.y);

        // Colar bounding box com a ledge no x e ver no editor quanto que offset que precisa no y
        base.player.SetState(new PlayerHangingState(base.player));
    }
}
