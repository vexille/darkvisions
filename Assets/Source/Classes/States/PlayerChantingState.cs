﻿using UnityEngine;
using System.Collections;

public class PlayerChantingState : State {

    public PlayerChantingState(Player player)
        : base(player, new ChantingInputHandler(player), new CommandQueue(player.gameObject)) {

    }

    public override void OnEnter() {
        Animator anim = base.player.gameObject.transform.Find("AFuckupOffset").GetComponentInChildren<Animator>();
        if (anim != null) {
            anim.SetBool("Chanting", true);
            anim.SetTrigger("StartChanting");
        }
    }

    public override void OnExit() {
        Animator anim = base.player.gameObject.transform.Find("AFuckupOffset").GetComponentInChildren<Animator>();
        if (anim != null) {
            anim.SetBool("Chanting", false);
        }
    }

}
