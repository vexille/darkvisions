﻿using UnityEngine;
using System.Collections;

public class PlayerClimbingState : State {

    private bool climbingUp;
    private float startingGravityScale;
    private Animator playerAnimator;
    private bool enteredClimbingAnimation = false;

    public PlayerClimbingState(Player player, bool climbingUp)
        : base(player, null, null) {
            this.climbingUp = climbingUp;
            this.playerAnimator = player.gameObject.transform.Find("AFuckupOffset").GetComponentInChildren<Animator>();
    }

    public override void Update() {
        
    }

    public override void FixedUpdate() {
        if (playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("ClimbingUp")) {
            if (!enteredClimbingAnimation) {
                enteredClimbingAnimation = true;
            }
        } else if (enteredClimbingAnimation) {
            base.player.SetState(new PlayerGroundedState(player));
        }
    }

    public override void OnEnter() {
        Rigidbody2D rigidbody = base.player.GetComponent<Rigidbody2D>();
        if (rigidbody != null) {
            this.startingGravityScale = rigidbody.gravityScale;
            rigidbody.gravityScale = 0f;
            rigidbody.velocity = new Vector2(0f, 0f);
        }

        if (climbingUp) {
            Animator anim = base.player.gameObject.transform.Find("AFuckupOffset").GetComponentInChildren<Animator>();

            if (anim != null) {
                anim.SetTrigger("ClimbUp");
            }

            //base.player.SetStateWithDelay(new PlayerGroundedState(player), delay);
        } else {

        }
    }

    public override void OnExit() {
        if (climbingUp) {
            Vector3 position = base.player.transform.position;
            position.x += 0.943353f * Mathf.Sign(base.player.transform.localScale.x);
            position.y += 4.215451f;
            base.player.transform.position = position;

            // Workaround for something -- remove this eventually
            Animator anim = base.player.gameObject.transform.Find("AFuckupOffset").GetComponentInChildren<Animator>();
            if (anim != null) {
                anim.SetTrigger("Release");
            }
        }

        LedgeCheck ledgeChecker = base.player.GetComponentInChildren<LedgeCheck>();
        if (ledgeChecker != null) {
            ledgeChecker.TouchingLedge = false;
        }

        Rigidbody2D rigidbody = base.player.GetComponent<Rigidbody2D>();
        if (rigidbody != null) {
            rigidbody.gravityScale = this.startingGravityScale;
        }
    }

}
