﻿using UnityEngine;
using System.Collections;

public class ChantCommand : Command {

    public override void Execute(GameObject gameObject) {
        if (SceneControl.Instance != null) {
            SceneControl.Instance.IncreaseDarknessLevel();
        }
    }

}
