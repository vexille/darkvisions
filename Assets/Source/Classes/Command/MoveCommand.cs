﻿using UnityEngine;
using System.Collections;

public class MoveCommand : Command {

    private float moveForce;
    private float maxSpeed;
    private float maxRunSpeed;
    private float horizontal;
    private bool running;

    public MoveCommand() {
        this.moveForce = 0f;
        this.maxSpeed = 0f;
        this.maxSpeed = 0f;
        this.horizontal = 0f;
    }

    public override void Execute(GameObject gameObject) {
        Rigidbody2D rigidbody = gameObject.GetComponent<Rigidbody2D>();

        if (rigidbody != null) {
            float speed = running ? maxRunSpeed : maxSpeed;
            float forceFactor = running ? 1.5f : 1f;
            if (this.horizontal * rigidbody.velocity.x < speed)
                rigidbody.AddForce(Vector2.right * this.horizontal * moveForce * forceFactor);

            // Enforce max speed
            if (Mathf.Abs(rigidbody.velocity.x) > speed)
                rigidbody.velocity = new Vector2(Mathf.Sign(rigidbody.velocity.x) * speed, rigidbody.velocity.y);

            // Set speed for walking/running animation
            Animator anim = gameObject.transform.Find("AFuckupOffset").GetComponentInChildren<Animator>();
            if (anim != null) {
                anim.SetFloat("Speed", Mathf.Abs(speed * horizontal));
            }
        }

        FlipControl flipControl = gameObject.GetComponent<FlipControl>();
        if (flipControl != null) {
            flipControl.CheckFlip(horizontal);
        }
    }


    public float MoveForce {
        get { return this.moveForce; }
        set { this.moveForce = value; }
    }

    public float MaxSpeed {
        get { return this.maxSpeed; }
        set { this.maxSpeed = value; }
    }

    public float MaxRunSpeed {
        get { return this.maxRunSpeed; }
        set { this.maxRunSpeed = value; }
    }

    public float Horizontal {
        get { return this.horizontal; }
        set { this.horizontal = value; }
    }

    public bool Running {
        get { return this.running; }
        set { this.running = value; }
    }
	
}
