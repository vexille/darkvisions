﻿using UnityEngine;
using System.Collections;

public class JumpCommand : Command {

	private float jumpForce;
    private bool running;

    public JumpCommand() {
        this.jumpForce = 0f;
        this.running = false;
    }

    public JumpCommand(float jumpForce) {
        this.jumpForce = jumpForce;
    }

    public override void Execute(GameObject gameObject) {
        Rigidbody2D rigidbody = gameObject.GetComponent<Rigidbody2D>();
        if (rigidbody != null) {
            float xFactor = running ? 1f : 0.5f;
            float direction = rigidbody.velocity.x != 0f ? Mathf.Sign(rigidbody.velocity.x) : 0f;
            rigidbody.velocity = new Vector2(0f, 0f);
            rigidbody.AddForce(new Vector2(jumpForce * xFactor * direction, jumpForce));
        }
    }

    public float JumpForce {
        get { return this.jumpForce; }
        set { this.jumpForce = value; }
    }

    public bool Running {
        get { return this.running; }
        set { this.running = value; }
    }

}
