﻿using UnityEngine;
using System.Collections;

abstract public class Command {

    abstract public void Execute(GameObject gameObject);
	
}
