﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CommandQueue {

    private List<Command> updateQueue;
    private List<Command> fixedQueue;
    private GameObject host;

    public CommandQueue(GameObject host) {
        this.updateQueue = new List<Command>();
        this.fixedQueue = new List<Command>();
        this.host = host;
    }

    public void Update() {
        foreach (Command command in updateQueue) {
            command.Execute(host);
        }

        updateQueue.Clear();
    }

    public void FixedUpdate() {
        foreach (Command command in fixedQueue) {
            command.Execute(host);
        }

        fixedQueue.Clear();
    }

    public void AddCommand(Command command) {
        updateQueue.Add(command);
    }

    public void AddCommand(Command command, bool fixedUpdate) {
        if (fixedUpdate) {
            fixedQueue.Add(command);
        } else {
            this.AddCommand(command);
        }
    }
}
