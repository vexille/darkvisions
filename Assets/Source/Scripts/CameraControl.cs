﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {

	public Transform player;
    public bool activateCameraFollow = true;
    public bool centerOnStart = true;
	public float trackingSpeed = 2f;
	public float maxRange = 1.5f;

	public Vector2 displacement = new Vector2(0, 0);

    private float initialX;
    //private float forwardTracking = 8f;

    void Awake() {
        if (this.centerOnStart) {
            Vector3 position = transform.position;
            position.x = player.position.x;
            transform.position = position;
        }

        this.initialX = transform.position.x;
    }

	void Update () {
		float z = transform.position.z;
		Vector3 pos = player.position;
        pos.y = transform.position.y;

        //FlipControl flipControl = player.GetComponent<FlipControl>();
        //if (flipControl != null) {
        //    float direction = flipControl.facingRight ? 1f : -1f;
        //    pos.x += forwardTracking * direction;
        //}

		Vector3 cameraPos;

        if (activateCameraFollow) {
            cameraPos = Vector3.Lerp(transform.position, pos, trackingSpeed * Time.deltaTime);
        } else {
            cameraPos = transform.position;
        }

		cameraPos.x += displacement.x;
		cameraPos.y += displacement.y;
		cameraPos.z = z;

        if (cameraPos.x < this.initialX) {
            cameraPos.x = this.initialX;
        }

		transform.position = cameraPos;

		displacement.x = 0f;
		displacement.y = 0f;
	}
}
