﻿using UnityEngine;
using System.Collections;

public class SceneControl : MonoBehaviour {

    public int nextLevel = 1;

    private static SceneControl instance;
    private float darknessLevel;
    private float maxDarknessLevel;
    private float changeRate;
    private float deadlyThreshold;

    void Awake() {
        this.darknessLevel = 0f;
        this.maxDarknessLevel = 10f;
        this.changeRate = 1f; // Per second
        this.deadlyThreshold = 0.5f;

        SceneControl.instance = this;
    }

	void Update () {
        if (Input.GetButtonDown("Restart")) {
            Application.LoadLevel(Application.loadedLevel);
        }

        this.darknessLevel -= changeRate * Time.deltaTime;
        if (this.darknessLevel < 0f) {
            this.darknessLevel = 0f;
        }
	}

    void OnDestroy() {
        SceneControl.instance = null;
    }

    public void IncreaseDarknessLevel() {
        this.darknessLevel += changeRate * 4f * Time.deltaTime;
        if (this.darknessLevel > this.maxDarknessLevel) {
            this.darknessLevel = this.maxDarknessLevel;
        }
    }

    public float DarknessLevel {
        get { return this.darknessLevel; }
        set {
            this.darknessLevel = value;
            if (this.darknessLevel > this.maxDarknessLevel) {
                this.darknessLevel = this.maxDarknessLevel;
            }
        }
    }

    public float MaxDarknessLevel {
        get { return this.maxDarknessLevel; }
    }

    public static SceneControl Instance {
        get { return SceneControl.instance; }
    }

    public bool IsDeadly {
        get { return this.darknessLevel >= this.deadlyThreshold; }
    }
}
