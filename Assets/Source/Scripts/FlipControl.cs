﻿using UnityEngine;
using System.Collections;

public class FlipControl : MonoBehaviour {

    [HideInInspector]
    public bool facingRight;

    void Awake() {
        Vector3 scale = transform.localScale;

        facingRight = scale.x > 0f;
    }

    public void CheckFlip(float direction) {
        if ((direction < 0f && facingRight) || (direction > 0f && !facingRight))
            Flip();
    }

    void Flip() {
        facingRight = !facingRight;

        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }
	
}
