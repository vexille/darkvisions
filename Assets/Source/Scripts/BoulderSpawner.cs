﻿using UnityEngine;
using System.Collections;

public class BoulderSpawner : MonoBehaviour {

    public Transform boulderPrefab;
    public float spawnInterval = 1f;

    private float elapsed = 0f;
    	
	void Update () {
        this.elapsed += Time.deltaTime;
        if (this.elapsed >= spawnInterval) {
            Transform boulder = Instantiate(boulderPrefab) as Transform;
            boulder.position = transform.position;

            this.elapsed = 0f;
        }
	}
	
}
