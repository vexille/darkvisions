﻿using UnityEngine;
using System.Collections;

public class Parallax : MonoBehaviour {

    public float depth = 2f;

    private const float MAX_DEPTH = 5f;
    private Vector3 lastPosition;
    private Vector3 lastCameraPosition;

	void Start () {
        this.lastPosition = transform.position;
        this.lastCameraPosition = Camera.main.transform.position;
	}
	
	void Update () {
        Vector3 currentCameraPosition = Camera.main.transform.position;
        if (this.lastCameraPosition != currentCameraPosition) {
            Vector3 deltaPosition = currentCameraPosition - this.lastCameraPosition;
            float depthFactor = (1f / MAX_DEPTH) * depth;

            Vector3 resultingPosition = this.lastPosition + (deltaPosition * depthFactor);

            this.transform.position = this.lastPosition = resultingPosition;
            this.lastCameraPosition = currentCameraPosition;
        }
	}
	
}
