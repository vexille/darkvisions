﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour {

    public float speed;
    public float jumpForce = 1000f;
    public float moveForce = 365f;
    public float maxSpeed = 5f;
    public float maxRunSpeed = 8f;

    private State currentState;
    private bool dead = false;

	void Awake() {
        this.currentState = new PlayerGroundedState(this);
	}

    void Update() {
        if (this.currentState != null)
            this.currentState.Update();
    }
	
	void FixedUpdate () {
        if (this.currentState != null)
            this.currentState.FixedUpdate();
	}

    public void SetState(State newState) {
        this.currentState.OnExit();
        this.currentState = newState;
        this.currentState.OnEnter();
    }

    public void SetStateWithDelay(State newState, float delay) {
        StartCoroutine(DelayForState(newState, delay));
    }

    public State CurrentState {
        get { return this.currentState; }
    }

    private System.Collections.IEnumerator DelayForState(State newState, float delay) {
        yield return new WaitForSeconds(delay);
        this.SetState(newState);
    }
    
    void OnCollisionEnter2D(Collision2D collision) {
        if (collision.collider.CompareTag("Envirokill")) {
            Die();
        }
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.CompareTag("Envirokill")) {
            Die();
        }
    }

    private void Die() {
        if (!this.dead) {
            this.currentState = null;
            this.dead = true;
            Animator anim = transform.Find("AFuckupOffset").GetComponentInChildren<Animator>();
            if (anim != null) {
                anim.SetTrigger("Envirodeath");
            }

            StartCoroutine(DieIn(1f));
        }
    }

    private System.Collections.IEnumerator DieIn(float delay) {
        yield return new WaitForSeconds(delay);
        Application.LoadLevel(Application.loadedLevel);
    }
}
