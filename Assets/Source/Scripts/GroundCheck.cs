﻿using UnityEngine;
using System.Collections;

public class GroundCheck : MonoBehaviour {

    private bool grounded = false;

    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.CompareTag("Ground"))
            grounded = true;
    }

    void OnTriggerExit2D(Collider2D collider) {
        if (collider.CompareTag("Ground"))
            grounded = false;
    }

    public bool Grounded {
        get { return this.grounded; }
    }
}
