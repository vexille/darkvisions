﻿using UnityEngine;
using System.Collections;

public class ShowWhenDeadly : MonoBehaviour {

    private ParticleSystem particles;
    private Light light;
    private float startingLightIntensity;

    void Awake() {
        this.particles = GetComponentInChildren<ParticleSystem>();
        this.light = GetComponentInChildren<Light>();
        if (this.light != null) {
            this.startingLightIntensity = light.intensity;
        }
    }

	void Update () {
		float t = SceneControl.Instance.DarknessLevel / SceneControl.Instance.MaxDarknessLevel;

        if (renderer != null) {
            renderer.enabled = SceneControl.Instance.IsDeadly;
            SpriteRenderer spriteRenderer = renderer as SpriteRenderer;
            if (spriteRenderer != null) {
                Color color = spriteRenderer.color;
                color.a = t;
                spriteRenderer.color = color;
            }
        }

        if (this.particles != null) {
            this.particles.enableEmission = SceneControl.Instance.IsDeadly;
			if (SceneControl.Instance.IsDeadly) {
				this.particles.emissionRate = Mathf.Lerp(0f, 150f, t);
			}
		}

        if (this.collider2D != null)
            this.collider2D.enabled = SceneControl.Instance.IsDeadly;

        if (this.light != null) {
            this.light.enabled = SceneControl.Instance.IsDeadly;
            this.light.intensity = Mathf.Lerp(0f, this.startingLightIntensity, t);
        }
	}
	
}
