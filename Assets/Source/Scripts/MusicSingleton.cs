﻿using UnityEngine;
using System.Collections;

public class MusicSingleton : MonoBehaviour {

    private static MusicSingleton instance = null;
    public static MusicSingleton Instance {
        get { return instance; }
    }

    void Awake() {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
            return;
        } else {
            instance = this;
        }

        DontDestroyOnLoad(this.gameObject);

        MusicSingleton.PlayBackgroundMusic();
    }

    public static void PlayBackgroundMusic() {
        if (Instance == null) {
            Debug.LogError("Erro: MusicSingleton null");
            return;
        }

        AudioSource source = Instance.gameObject.GetComponent<AudioSource>();
        if (!source.isPlaying) {
            source.Play();
        }
    }
}