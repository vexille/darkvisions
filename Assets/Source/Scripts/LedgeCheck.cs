﻿using UnityEngine;
using System.Collections;

public class LedgeCheck : MonoBehaviour {

    private bool touchingLedge = false;
    private Collider2D ledgeCollider;

    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.CompareTag("Ledge")) {
            touchingLedge = true;
            ledgeCollider = collider;
        }
    }

    void OnTriggerExit2D(Collider2D collider) {
        if (collider.CompareTag("Ledge")) {
            touchingLedge = false;
            ledgeCollider = null;
        }
    }

    public bool TouchingLedge {
        get { return this.touchingLedge; }
        set { this.touchingLedge = value; }
    }

    public Collider2D LedgeCollider {
        get { return this.ledgeCollider; }
    }
}
