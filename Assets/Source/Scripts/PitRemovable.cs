﻿using UnityEngine;
using System.Collections;

public class PitRemovable : MonoBehaviour {

    void Update() {
        if (!this.collider2D.enabled && transform.position.y < -30f)
            Destroy(gameObject);
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.CompareTag("Pit")) {
            Destroy(gameObject);
        }
    }
	
}
