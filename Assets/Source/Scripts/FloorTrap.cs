﻿using UnityEngine;
using System.Collections;

public class FloorTrap : MonoBehaviour {

    public float inactiveDuration = 1f;
    public float initialDelay = 0f;

    private ParticleSystem particles;
    private bool trapActive;
    private float elapsed;
    private float activeDuration;
    private bool firstExecution;

    void Awake() {
        this.particles = GetComponentInChildren<ParticleSystem>();
        this.trapActive = false;
        this.collider2D.enabled = false;
        this.elapsed = 0f;
        this.activeDuration = particles.duration;
        this.firstExecution = true;
    }
    	
	void Update () {
        if (this.trapActive) {
            this.elapsed += Time.deltaTime;

            if (!SceneControl.Instance.IsDeadly) {
                DeactivateTrap();
                return;
            }

            if (this.collider2D.enabled && elapsed >= activeDuration) {
                this.collider2D.enabled = false;
                this.elapsed = 0f;
            } 
            
            else if (!this.collider2D.enabled) {
                if (this.firstExecution) {
                    if (this.elapsed >= this.initialDelay) {
                        this.collider2D.enabled = true;
                        this.particles.Play();
                        this.elapsed = 0f;
                        this.firstExecution = false;
                    }
                }

                else if (this.elapsed >= this.inactiveDuration) {
                    this.collider2D.enabled = true;
                    this.particles.Play();
                    this.elapsed = 0f;
                }
            }

        } else if (SceneControl.Instance.IsDeadly) {
            ActivateTrap();
        }
	}

    private void ActivateTrap() {
        this.trapActive = true;
        this.elapsed = 0f;
        this.firstExecution = true;
    }

    private void DeactivateTrap() {
        this.trapActive = false;
        this.collider2D.enabled = false;
        this.particles.Stop();
        this.elapsed = 0f;
    }
	
}
