﻿using UnityEngine;
using System.Collections;

public class TriggerChecker : MonoBehaviour {

    private bool touchingDoor = false;
    private bool touchingAltar = false;

    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.CompareTag("Pit")) {
            Application.LoadLevel(Application.loadedLevel);
        }

        CheckState(collider, ref this.touchingDoor, "Door", true);
        CheckState(collider, ref this.touchingAltar, "Altar", true);
    }

    void OnTriggerExit2D(Collider2D collider) {
        CheckState(collider, ref this.touchingDoor, "Door", false);
        CheckState(collider, ref this.touchingAltar, "Altar", false);
    }

    private void CheckState(Collider2D collider, ref bool flag, string tag, bool state) {
        if (collider.CompareTag(tag)) {
            flag = state;
        }
    }

    public bool TouchingDoor {
        get { return this.touchingDoor; }
        set { this.touchingDoor = value; }
    }

    public bool TouchingAltar {
        get { return this.touchingAltar; }
        set { this.touchingAltar = value; }
    }
}
